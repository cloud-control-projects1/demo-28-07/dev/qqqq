package com.company.qqqq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class QqqqApplication {

    public static void main(String[] args) {
        SpringApplication.run(QqqqApplication.class, args);
    }
}
